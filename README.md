# AWS Architects & Engineers Meetup

Presentation for the AWS architects & engineers meetup in Seattle November 2015. Made with the awesome revealjs. [Check out the live presentation](http://aws-meetup.aerobatic.io).
